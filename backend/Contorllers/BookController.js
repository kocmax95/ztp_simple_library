import BookService from '../Services/BookServices'

export const bookController = (app)=> {

    const bookService = new BookService();

    app.get('/api/book', async (req, res)=> {
        try {

            const books = await bookService.GetAllBooks();
            console.log(books);
        res.send(books)
        } catch (error) {
            res.send(error)
        }
    })

    app.post('/api/book', async (req,res) => {
        try {
            const createdBook = await bookService.SaveBook(req.body);
            res.send(createdBook);
        } catch (error) {
            res.send(`Nie udało się zapisać książki. Błąd: ${error}`)
        }
    })
}

// export default bookController;
module.exports=bookController;
