import BookRepository from '../Repositories/BookRepository';
import {createBookModel} from '../Models/Book'

export default class BookService {
    constructor(){
        this.bookRepository = new BookRepository();
    }
    
    GetAllBooks = async () => {
        const allBooks = await this.bookRepository.GetBooks();
        return allBooks;
    }

    SaveBook = async (bookDto) => {
        const bookModel = createBookModel(bookDto);
        const createdBook = await this.bookRepository.Save(bookModel);
        return createdBook;
    }

    // DeleteBook = async () => {
        
    // }
}