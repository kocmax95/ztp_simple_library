const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const mongoose = require('mongoose')

const bookController =  require('./Contorllers/BookController');

app.use(cors());
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }));

const port = 8080;

mongoose.connect('mongodb://localhost:27017/library', {
    useNewUrlParser: true,
    useCreateIndex: true
}, (err) => {
    if (!err) {
        console.log(`Connection with database: OK`);
    } else {
        console.log(err);
    }
})

bookController(app);

app.listen(port, () => {
    console.log(`server listening on port: ${port}`);
})
