const mongoose = require('mongoose');

const BookSchema = mongoose.Schema({
    title: String,
    author: String,
    year: String,
    description: String,
    creationDate: String
});

export const createBookModel = (bookDto) => {
    return {
        title: bookDto.title,
        author: bookDto.author,
        year: bookDto.year,
        description: bookDto.description,
        creationDate: new Date()
    }
}

export const Books = mongoose.model('Books', BookSchema,'Books');