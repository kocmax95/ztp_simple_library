import {Books} from '../Models/Book';

class BookRepository {

    GetBooks = async () => {
        const books = await Books.find();
        return books;
    }

    Save = async (book) => {
        const saveBook = await Books(book).save();
        return saveBook;
    }

}

export default BookRepository;


//Repozytoriu obsługuje baze danych.
//Services łaczy kontrolery z repozytorium
// Controler obsługuje rządania, kontroluje przepływ procesów (woła do serwisów)