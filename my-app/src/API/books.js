import axios from 'axios';

export const allBooks = async ()=> {
    console.log(`odpalam wyświetlanie książek`);
    let result = await axios.get('http://localhost:8080/api/book')
    console.log(result);
    return result;
}

export const addBook = async (addBookData) => {
    let result = await axios.post('http://localhost:8080/api/book', addBookData);
    
    return result;
}