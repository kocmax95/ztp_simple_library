import React, { Component } from 'react';
import '../node_modules/bootstrap/dist/css/bootstrap.css'
import AddBook from './components/AddBook';
import Header from './components/Header'
import Main from './components/Main'
import {
  BrowserRouter as Router,
  // Switch,
  Route,
  // Link
} from "react-router-dom";

class App extends Component {
  state = {

  }

  render() {
    return (

      <Router>
        <Header/>
        <Route exact path="/" component={Main}/>
        <Route path="/addBook" component={AddBook}/>
      </Router>

    )
  }
}
export default App;
