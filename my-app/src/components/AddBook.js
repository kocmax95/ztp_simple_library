import React, {Component} from 'react';
import {addBook} from '../API/books'


class AddBook extends Component {
    state = {
        title: "",
        author: "",
        year:"",
        description: "",
    
      }
    
    
      handleInputTitle = (e) => {
        this.setState({
          title: e.target.value
        })
      }
    
      handleInputAuthor = (e) => {
        this.setState({
          author: e.target.value
        })
      }
    
      handleInputYear = (e) => {
        this.setState({
          year: e.target.value
        })
      }
    
      handleInputDescription = (e) => {
        this.setState({
          description: e.target.value
        })
      }
    
      handleBtnSubmit = async (e) => {
        e.preventDefault();
    
        const addBookData = {
          title: this.state.title,
          author: this.state.author,
          year: this.state.year,
          description: this.state.description
        }
    
        const result = await addBook(addBookData);
        console.log(`wysyłam dane : ${result}`);
    
      }
    render() { 
        return(
            <div className="container card bg-light p-5 mt-5 col-3">
                    <form className="">
                        <h3>Dodawanie nowej książki</h3>
                        <div className="form-group">
                            <label htmlFor="title">Tytuł:</label>
                            <input type="text" onChange={this.handleInputTitle} value={this.state.title} className="form-control" id="title" />
                        </div>

                        <div className="form-group">
                            <label htmlFor="author">Autor:</label>
                            <input type="text" onChange={this.handleInputAuthor} value={this.state.author} className="form-control" id="author" />
                        </div>

                        <div className="form-group">
                            <label htmlFor="year">Rok:</label>
                            <input type="text" onChange={this.handleInputYear} value={this.state.year} className="form-control" id="year" />
                        </div>

                        <div className="form-group">
                            <label htmlFor="description">Opis:</label>
                            <textarea type="text" onChange={this.handleInputDescription} value={this.state.description} className="form-control" id="description" />
                        </div>

                        <button type="submit" onClick={this.handleBtnSubmit} className="btn btn-primary">Dodaj</button>
                    </form>
                </div>
        )
    }
}
 
export default AddBook;