import React from 'react';
import {Link} from "react-router-dom";

import 'bootstrap/dist/css/bootstrap.css'

class Header extends React.Component {
    state = {}
    render() {
        return (
            <>
                    <div className="container d-flex justify-content-between ">
                        <Link to='/'><h1 className="display-4">Simple Library</h1></Link>
                        <div>
                            <Link to='/addBook'><button className="btn btn-light justivy-content mt-3">Dodaj Książkę</button></Link>
                        </div>
                    </div>
            </>
        )
    }

}

export default Header;





