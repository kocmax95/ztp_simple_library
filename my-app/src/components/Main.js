import React, { Component } from 'react';
import { allBooks } from '../API/books'

class Main extends Component {
    state = {
        title: "",
        author: "",
        year: "",
        description: "",
        result: []
    }
    componentDidMount = async () => {
        // allBooks();

        const booksData = await allBooks();

        this.setState({
            result: booksData.data
        })
        console.log(this.state.result);
    }
    render() {
        const books = this.state.result.map((book) =>
            <div className="card col-4">
                <div className="card-header d-flex justify-content-between">
                    <div className="pt-2 pr-2 font-weight-bold">{book.title}</div>
                    <div className="pt-2 pr-2">{book.year}</div>
                </div>
                <div className="card-body">
                    {book.description}
                </div>
                <div className="card-footer">
                    <button className="btn btn-danger">Usuń</button>
                </div>
            </div>)
        return (
            
            <div className="container d-flex flex-wrap pt-5">
                {books}
                
            </div>
        );
    }
}

export default Main;